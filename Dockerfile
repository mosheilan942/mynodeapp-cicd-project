FROM node:8.11

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY ./app/package.json /usr/src/app/
RUN npm install

COPY ./app /usr/src/app

# replace this with your application's default port
EXPOSE 8888
CMD [ "npm", "start" ]